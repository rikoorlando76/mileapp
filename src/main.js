import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import router from "./router";
import VueI18n from "./translate";
const app = createApp(App);
app.use(router);
app.use(VueI18n);
app.mount("#app");
