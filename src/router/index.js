import { createWebHistory, createRouter } from "vue-router";
import Home from "@/pages/Home.vue";
import Track from "@/pages/Track.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/track",
    name: "Track",
    component: Track,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
