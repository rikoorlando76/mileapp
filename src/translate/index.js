// import Vue from "vue";
import { createI18n } from "vue-i18n";

const messages = {
  en: {
    login: "Login",
    orgName: "Organization Name",
    emailOrUsername: "Email or Username",
    orgNamePlaceholder: "input your organization name",
    emailOrUsernamePlaceholder: "input your email or username",
    passwordPlaceholder: "input your password",
    notRegister: "Not registered yet?",
    moreInfo: "for more info",
    contactUs: "Contact us",
    toggleOffline: "Show offline driver",
    search: "Search",
    required: "please fill this field",
    wrongUser: "wrong username/email or password",
  },
  id: {
    login: "Masuk",
    orgName: "Nama Organisasi",
    emailOrUsername: "Email atau username",
    orgNamePlaceholder: "masukkan nama organisasi anda",
    emailOrUsernamePlaceholder: "masukkan email atau username anda",
    passwordPlaceholder: "masukkan password anda",
    notRegister: "Belum registrasi?",
    moreInfo: "untuk informasi lebih lanjut",
    contactUs: "Hubungi kami",
    toggleOffline: "Tampilkan driver offline",
    search: "Cari",
    required: "wajib diisi",
    wrongUser: "salah username/email atau password",
  },
};

const i18n = createI18n({
  locale: "id", // set locale
  fallbackLocale: "id", // set fallback locale
  messages, // set locale messages
});

export default i18n;
