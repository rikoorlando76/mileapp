export const listDriver = [
  {
    id: 1,
    position: {
      lat: -6.595853,
      lng: 106.789257,
    },
    content: {
      name: "driver A",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: true,
  },
  {
    lat: -6.598667,
    lng: 106.790717,
    id: 2,
    position: {
      lat: -6.598667,
      lng: 106.790717,
    },
    content: {
      name: "driver B",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: true,
  },
  {
    id: 3,
    position: {
      lat: -6.595,
      lng: 106.808753,
    },
    content: {
      name: "driver C",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: true,
  },
  {
    position: { lat: -6.605232, lng: 106.809354 },
    id: 4,
    content: {
      name: "driver D",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: true,
  },
  {
    position: {
      lat: -6.589288,
      lng: 106.800165,
    },
    id: 5,
    content: {
      name: "driver E",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: true,
  },
  {
    id: 6,
    position: {
      lat: -6.605402,
      lng: 106.795791,
    },
    content: {
      name: "driver F",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: false,
  },
  {
    id: 7,
    position: {
      lat: -6.598922,
      lng: 106.799355,
    },
    content: {
      name: "driver G",
      plat: "F 1320 rf",
      progress: "4/8",
      lastSeen: "22 June 2022, 5:00 PM",
    },
    status: false,
  },
];
