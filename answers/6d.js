const array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

function generateObj(arrKey, oriStr, objResult) {
  if(arrKey[0]){
    const sliced = arrKey.slice(1, arrKey.length)
    const objDeep = objResult[arrKey[0]]
    const objAsProps = typeof objDeep === 'object' ? objDeep : {}
    const objMerge = typeof objResult === 'object' ? objResult : {}
    return {
      ...objMerge,
      [arrKey[0]]: generateObj(sliced, oriStr, objAsProps)
    }
  }
  if(Object.keys(objResult).length){
    return objResult
  }
  return oriStr
 
}

function formatedData(arrCode) {
  let result = {}
  arrCode.forEach(el => {
    const splited = el.split('.')
    const obj = generateObj(splited, el, result)
    result= obj
  });
  return result
}
console.log(JSON.stringify(formatedData(array_code), null, 4));